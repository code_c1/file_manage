#include <stdio.h>

#include "file_func.h"

#define SIZE 100

void init_file(const char* const path);
void add_token(FILE *f);

int main(void) {
    char file_path[] = "portfolio.dat";
    init_file(file_path);
    FILE *f = fopen(file_path, "rb+");
    if (f == NULL) {
        puts("open file error");
    } else {
        fseek(f, 0, SEEK_SET);
        add_token(f);
    }

    return 0;
}

void init_file(const char* const path) {
    unsigned int i;
    Field blank_field = {"", "", 0.00, DEFAULT};

    FILE *f = fopen(path, "wb");
    if (f == NULL) {
        printf("open file error");
    } else {
        for (i = 0; i < SIZE; i++) {
            fwrite(&blank_field, sizeof(Field), 1, f);
        }
        fclose(f);
    }
}

void add_token(FILE *f) {
    Field temp_field;
    int temp;
    puts("Enter ID, name, balance and chain(1 - ETH, 2 - BSC, 3 - ARB)");
    fscanf(stdin, "%4s%9s%Lf%1d", temp_field.token_id, temp_field.token_name, &temp_field.balance, &temp);
    temp_field.chain = temp;
    fwrite(&temp_field, sizeof(Field), 1, f);
}