#ifndef FILE_FUNC_H
#define FILE_FUNC_H

#define LEN_NAME 10
#define LEN_ID 5

typedef enum {DEFAULT, ERC20, BEP20, ARB} Chain;

typedef struct data_field {
    char token_id[LEN_ID];
    char token_name[LEN_NAME];
    long double balance;
    Chain chain;
} Field;

#endif